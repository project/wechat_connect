<?php
namespace Drupal\wechat_connect\Plugin\WechatApplicationType;

use Drupal\wechat_connect\Plugin\WechatApplicationTypeBase;

/**
 * @WechatApplicationType(
 *   id = "web_site",
 *   label = @Translation("Web Site")
 * )
 */
class WebSite extends WechatApplicationTypeBase {

}