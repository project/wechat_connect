<?php
namespace Drupal\wechat_connect\Plugin\WechatApplicationType;

use Drupal\wechat_connect\Plugin\WechatApplicationTypeBase;

/**
 * @WechatApplicationType(
 *   id = "mobile_app",
 *   label = @Translation("MobileApp")
 * )
 */
class MobileApp extends WechatApplicationTypeBase {

}